# Engelsystem Shift Printer

This is a tool to print shifts from an Engelsystem instance using a thermal printer.

## Usage

Create the configuration file `shiftprinter.toml` by copying `shiftprinter.toml.example` and setting all variables.
Then run `gui.py`.
This tool requires Python 3.6 or newer, Qt 5 and the dependencies defined in `python-requirements/base.txt`.

## License

This project is licensed under the MIT License.
See LICENSE for details.
