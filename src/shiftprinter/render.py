# SPDX-License-Identifier: MIT

from .driver import DriverBase
from .esclient.schema import Shift, User
from abc import ABCMeta, abstractmethod
from datetime import datetime
from enum import Enum
from loguru import logger



## "Interface" of formatter
class RendererBase(metaclass=ABCMeta):
    @abstractmethod
    def print_single_shift(self, shift: Shift, angels: dict[int, User]) -> None:
        pass

    @abstractmethod
    def print_standby_angel_token(self, shift: Shift, angel: User) -> None:
        pass



## Implementations of formatters
class RawRenderer(RendererBase):
    def __init__(self, driver: DriverBase, base_url: str):
        self.driver = driver
        self.base_url = base_url


    def print_single_shift(self, shift: Shift, angels: dict[int, User]) -> None:
        logger.debug(f'Printing single shift {shift.id} with {len(angels)} angels.')
        with self.driver as d:
            d.line_spacing(60)

            ## Header (shift type)
            d.set(align='left', bold=True, width=2, height=2, custom_size=True)
            d.textln(shift.type)

            ## Quick link / QR code to shift
            d.set(align='center', bold=False, width=1, height=1, custom_size=True)
            d.qr(f'{self.base_url}/shifts?action=view&shift_id={shift.id}', size=4)

            ## Shift metadata
            d.set(align='left', bold=False, width=1, height=1, custom_size=True)
            d.textln(f'Title:      {shift.title}')
            d.textln(f'ID:         {shift.id}')
            d.textln(f'Start:      {shift.start}')
            d.textln(f'End:        {shift.end}')
            d.textln(f'Location:   {shift.location.name}')
            d.textln(f'Retrieved:  {datetime.now().isoformat(" ", "seconds")}')

            for job in sorted(shift.jobs.values()):
                ## Subheader (job type)
                d.ln()
                d.set(align='left', bold=True, width=1, height=1, custom_size=True)
                d.textln(job.angel_type.name)

                ## List of angels
                d.set(align='left', bold=False, width=1, height=1, custom_size=True)
                for angel in sorted(angels[x] for x in job.angels):
                    has_dect = len(angel.phone_numbers) > 0 and angel.phone_numbers[0] != '-'
                    dect = f'DECT {angel.phone_numbers[0]}' if has_dect else ''
                    d.textln(f'__  {angel.name}  [{angel.id}]  {dect}\n\n')
            d.cut()


    def print_standby_angel_token(self, shift: Shift, angel: User) -> None:
        raise NotImplementedError()



## Collection of formatters
class Renderer(Enum):
    raw = RawRenderer
