# SPDX-License-Identifier: MIT

from .esclient.schema import Shift
from .shiftprinter import ShiftPrinter
from PyQt6.QtCore import Qt
from PyQt6.QtGui import QKeySequence
from PyQt6.QtWidgets import QWidget, QApplication, QPushButton, QLabel, QVBoxLayout
from functools import partial
from loguru import logger
from signal import signal, SIGINT, SIG_DFL



## The first three alphanumerical rows of a QWERTY keyboard (w/o special
## characters), to be used as keyboard shortcuts.
_shortcuts = [
    '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
    'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p',
    'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l',
]


def exec_gui_blocking(shiftprinter: ShiftPrinter, fullscreen: bool = False) -> int:
    signal(SIGINT, SIG_DFL) ## Allow for Ctrl+C
    app = QApplication([]) ## Possible args: https://doc.qt.io/qt-5/qapplication.html#QApplication

    window = Window(shiftprinter)
    if fullscreen:
        window.showFullScreen()
    else:
        window.show()

    return app.exec()



class Window(QWidget):
    def __init__(self, shiftprinter: ShiftPrinter):
        QWidget.__init__(self)
        self.shiftprinter = shiftprinter
        self.buttons: list[QPushButton] = []
        self.setWindowTitle('Shift Printer')

        ## Create root layout
        self.btn_layout = QVBoxLayout(self)
        self.btn_layout.setSpacing(10)
        self.btn_layout.setAlignment(Qt.AlignmentFlag.AlignHCenter | Qt.AlignmentFlag.AlignTop)

        ## Add header
        header = QLabel('Print list of angles in a shift!\n(for support call Skruppy: 7578)')
        header.setAlignment(Qt.AlignmentFlag.AlignHCenter | Qt.AlignmentFlag.AlignTop)
        self.btn_layout.addWidget(header)

        ## Go to home screen
        self._display_home()


    def _display_home(self) -> None:
        logger.debug('Displaying home screen.')
        self._clear_buttons()

        btn = QPushButton('Retrieve shifts\n[press space]')
        btn.clicked.connect(self._display_shifts) # type: ignore
        btn.setShortcut(QKeySequence('space'))
        self._add_button(btn)


    def _display_shifts(self) -> None:
        logger.debug('Displaying list of shifts.')

        shifts = self.shiftprinter.find_shifts()
        if not shifts:
            ## Stop before deleting all buttons, if no button would be shown
            logger.info('No shifts found.')

        ## Remove old buttons
        self._clear_buttons()

        ## Add shift buttons
        for i, shift in enumerate(shifts):
            btn_title = f'{shift.start.strftime("%H:%M")} - {shift.end.strftime("%H:%M")}'
            if i < len(_shortcuts):
                shortcut = _shortcuts[i]
                btn = QPushButton(f'{btn_title}\n[press <{shortcut}>]')
                btn.setShortcut(QKeySequence(shortcut))
            else:
                btn = QPushButton(btn_title)
            btn.clicked.connect(partial(self._retrieve_and_print_shift, shift)) # type: ignore
            self._add_button(btn)

        ## Add back button
        btn = QPushButton(f'Go back\n[press ESC]')
        btn.setShortcut(QKeySequence.StandardKey.Cancel)
        btn.clicked.connect(self._display_home) # type: ignore
        self._add_button(btn)


    def _retrieve_and_print_shift(self, shift: Shift) -> None:
        logger.debug('Shift selected to be printed.')
        self.shiftprinter.print_shift(shift)
        self._display_home()


    def _add_button(self, btn: QPushButton) -> None:
        btn.setMinimumHeight(40)
        self.btn_layout.addWidget(btn)
        self.buttons.append(btn)


    def _clear_buttons(self) -> None:
        for btn in self.buttons:
            btn.deleteLater()
        self.buttons = []
