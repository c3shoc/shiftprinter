# SPDX-License-Identifier: MIT

from . import __version__
from .gui import exec_gui_blocking
from .shiftprinter import ShiftPrinter
from argparse import ArgumentParser
from loguru import logger
from pathlib import Path
from time import sleep
import sys



def main():
    ## Parse command line arguments
    parser = ArgumentParser(
        description='Print list of angeels in a shift within the Engelsystem on a thermal receipt printer.'
    )
    parser.add_argument(
        '-f', '--fullscreen',
        action='store_true',
        help='Open application in fullscreen mode'
    )
    parser.add_argument(
        '-c', '--config',
        type=Path,
        help='Load config file'
    )
    parser.add_argument(
        '-v', '--verbose',
        action='count',
        default=0
    )
    parser.add_argument(
        '-r', '--restart',
        type=int,
        nargs='?', const='3', default='0'
    )
    parser.add_argument(
        '-V', '--version',
        action = 'version',
        version = __version__
    )
    args = parser.parse_args()

    ## Setup logging
    logger.remove()
    logger.add(
        sys.stderr,
        format='<green>{time:HH:mm:ss}</green> [<level>{level:1.1}</level>] {message} <dim>-- {name}:{line}</dim>',
        level=logger.level('WARNING').no - 10*args.verbose
    )

    logger.info(f'Starting {parser.prog} v{__version__}')

    ## Instantiate program logic
    shiftprinter = ShiftPrinter(args.config)

    ## Enter eventloop
    while True:
        try:
            logger.debug('Entering eventloop.')
            ret = exec_gui_blocking(shiftprinter, args.fullscreen)
            logger.debug('Left eventloop. Terminating.')
            sys.exit(ret)
        except Exception:
            if args.restart <= 0:
                logger.exception('Unexpected error. Terminating.')
                sys.exit(1)
            else:
                logger.exception(f'Unexpected error. Restarting in {args.restart}s.')
                sleep(args.restart)
