# SPDX-License-Identifier: MIT

from importlib import metadata



__version__ = metadata.version(__package__)
