# SPDX-License-Identifier: MIT

from .driver import Driver, DriverBase
from .esclient import ESClient
from .esclient.schema import Shift, User
from .render import Renderer, RendererBase
from datetime import datetime, timedelta
from loguru import logger
from pathlib import Path
from typing import Any, MutableMapping, Optional
import tomllib



def _driver_from_config(config: MutableMapping[str, Any]) -> DriverBase:
    driver_config = config.get('driver', {'type': 'null'}).copy()

    name = driver_config.pop('type')
    try:
        return Driver[name].value(driver_config)
    except KeyError:
        choices = ' '.join((x.name for x in Driver))
        raise Exception(f'Unknown driver "{name}", choose one of: {choices}')


def _renderer_from_config(config: MutableMapping[str, Any], driver: DriverBase) -> RendererBase:
    name = config.get('renderer', 'raw')
    try:
        return Renderer[name].value(driver, config['base_url'])
    except KeyError:
        choices = ' '.join((x.name for x in Renderer))
        raise Exception(f'Unknown renderer "{name}", choose one of: {choices}')


def _find_config() -> Path:
    config_pathes = [
        'shiftprinter.toml',
        '~/.config/shiftprinter.toml',
        '/etc/shiftprinter.toml',
    ]

    for path_str in config_pathes:
        path = Path(path_str).expanduser()
        if path.is_file():
            return path

    raise Exception(f'Did not found any config files in {config_pathes}')


def _read_config(config_file: Optional[Path] = None) -> MutableMapping[str, Any]:
    if config_file is None:
        config_file = _find_config()

    logger.info(f'Reading config from {config_file}.')
    with config_file.open('rb') as f:
        return tomllib.load(f)



class ShiftPrinter:
    def __init__(self, config_file: Optional[Path]=None):
        self.config = _read_config(config_file)

        self.fake_time = datetime.fromisoformat(self.config['fake_time']) if 'fake_time' in self.config else None
        self.esclient = ESClient(self.config['base_url'])
        self.esclient.login(self.config['username'], self.config['password'])

        driver = _driver_from_config(self.config)
        self.renderer = _renderer_from_config(self.config, driver)


    def find_shifts(self) -> list[Shift]:
        now = datetime.now() if self.fake_time is None else self.fake_time
        start = now - timedelta(hours=self.config['before'])
        end   = now + timedelta(hours=self.config['after'])

        shifts = self.esclient.list_shifts(
            earliest_start = start, latest_start = end,
            locations = self.config['locations'], angel_types = [1,2],
            show_available = True, show_unavailable = True
        )

        return sorted(filter(self._shift_filter, shifts))


    def print_shift(self, shift: Shift) -> None:
        shifts = self.esclient.list_shifts(
            earliest_start = shift.start, latest_start = shift.start,
            locations = [shift.location.id], angel_types = [1,2],
            show_available = True, show_unavailable = True
        )
        shift = next(filter(lambda x: x.id == shift.id, shifts))

        angels: dict[int, User] = {}
        for job in shift.jobs.values():
            for id in job.angels:
                angels[id] = self.esclient.get_user(id)

        self.renderer.print_single_shift(shift, angels)


    def _shift_filter(self, shift: Shift) -> bool:
        return shift.type == self.config['shift_type']
