# SPDX-License-Identifier: MIT

from dataclasses import dataclass
from datetime import datetime, timedelta, date
from typing import Optional



@dataclass(frozen=True, order=True, kw_only=True)
class AngelType:
    name: str
    id: int

@dataclass(frozen=True, order=True, kw_only=True)
class AngelColleague:
    name: str
    id: int
    is_freeloader: bool

@dataclass(frozen=True, order=True, kw_only=True)
class AngelShort:
    name: str
    id: int

@dataclass(frozen=True, order=True, kw_only=True)
class Location:
    name: str
    id: int

@dataclass(frozen=True, order=True, kw_only=True)
class Job:
    angel_type: AngelType
    angels: dict[int, AngelColleague]
    open_spots: Optional[int]

@dataclass(frozen=True, order=True, kw_only=True)
class Shift:
    start: datetime
    end: datetime
    type: str
    location: Location
    title: Optional[str]
    id: int
    jobs: dict[int, Job]

@dataclass(frozen=True, order=True, kw_only=True)
class ShiftEntry:
    shift: Shift
    angel: AngelShort
    id: int
    score_duration: timedelta
    comment: Optional[bool]
    is_freeloaded: bool
    freeloading_comment: Optional[str]

@dataclass(frozen=True, order=True, kw_only=True)
class Worklog:
    date: date
    id: Optional[int]
    angel: AngelShort
    score_duration: timedelta
    comment: Optional[bool]
    creator: AngelShort
    creation_date: datetime

@dataclass(frozen=True, order=True, kw_only=True)
class User:
    name: str
    id: int
    pronoun: Optional[str]
    phone_numbers: list[str]
    angel_types: dict[int, AngelType]
    roles: list[str]
    api_key: Optional[str]
    shifts: Optional[dict[int, ShiftEntry]]
    worklogs: Optional[list[Worklog]]
