# SPDX-License-Identifier: MIT

from bs4 import Tag
from datetime import datetime, timedelta, date, time
from urllib.parse import parse_qs, urlparse



def parse_date(s: str) -> date:
    return datetime.strptime(s, '%m-%d').date()

def parse_date_full(s: str) -> date:
    return datetime.strptime(s, '%Y-%m-%d').date()

def parse_time(s: str) -> time:
    return datetime.strptime(s, '%H:%M').time()

def parse_datetime(s: str) -> date:
    return datetime.strptime(s, '%Y-%m-%d %H:%M')

def next_time(base_dt: datetime, time: time) -> datetime:
    next_dt = datetime.combine(base_dt.date(), time)
    if next_dt < base_dt:
        next_dt += timedelta(days=1)
    return next_dt

def id_from_link(link: Tag, query_parameter_name: str) -> int:
    href = link['href']
    assert type(href) == str
    query_string = urlparse(href).query
    query_parameters = parse_qs(query_string)
    return int(query_parameters[query_parameter_name][0])
