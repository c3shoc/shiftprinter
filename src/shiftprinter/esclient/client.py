# SPDX-License-Identifier: MIT

from .parser import list_shifts, get_user
from .schema import Shift, User
from bs4 import BeautifulSoup, Tag
from datetime import datetime
from http.cookiejar import DefaultCookiePolicy, Cookie
from httpx import Client
from typing import Iterator, Optional
from urllib.request import Request



class InsecureCookiePolicy(DefaultCookiePolicy):
    def set_ok(self, cookie: Cookie, request: Request):
        ## Remove `secure` tag from cookie, wich limits cookies too HTTPS
        ## requests, but is set regardles of transport encryption. Allows for
        ## testing ESClient from local setups w/o TLS while not impacting
        ## security in production.
        cookie.secure = False
        return super().set_ok(cookie, request)



class ESClient:
    def __init__(self, base_url: str):
        self.client = Client(base_url=base_url)
        self.client.cookies.jar.set_policy(InsecureCookiePolicy())


    def login(self, username: str, password: str) -> None:
        ## Step 1: Aquire CSRF token
        response = self.client.get('/login')
        response.raise_for_status()
        soup = BeautifulSoup(response.text, 'lxml')
        csrf_token_input = soup.find('input', attrs={'name': '_token'})
        assert csrf_token_input is not None and type(csrf_token_input) == Tag
        csrf_token = csrf_token_input['value']

        ## Step 2: Do actual login
        form_data = {
            '_token': csrf_token,
            'login': username,
            'password': password,
            'submit': '',
        }
        response = self.client.post('/login', data=form_data)
        if response.status_code != 302:
            raise Exception(f'failed to login as "{username}"')
        ## Returns nothing, since the login session is stored in the requests cookie jar


    def list_shifts(
        self,
        earliest_start: Optional[datetime] = None, latest_start: Optional[datetime] = None,
        locations: Optional[list[int]] = None, angel_types: Optional[list[int]] = None,
        show_available: Optional[bool] = None, show_unavailable: Optional[bool] = None
    ) -> Iterator[Shift]:
        ## Prepare query/filter
        params: dict[str, str|list[str]|list[int]] = {
            'p': 'user_shifts',
        }

        if earliest_start is not None:
            params['start_day'] = earliest_start.strftime('%Y-%m-%d')
            params['start_time'] = earliest_start.strftime('%H:%M')

        if latest_start is not None:
            params['end_day'] = latest_start.strftime('%Y-%m-%d')
            params['end_time'] = latest_start.strftime('%H:%M')

        if locations is not None:
            params['locations[]'] = locations

        if angel_types is not None:
            params['types[]'] = angel_types

        availability: list[str] = []
        if show_available is not None:
            availability.append('1')
        if show_unavailable is not None:
            availability.append('0')
        if len(availability) > 0:
            params['filled[]'] = availability

        ## Request shifts page
        response = self.client.get('/user-shifts', params=params)
        response.raise_for_status()

        assert earliest_start is not None and latest_start is not None ## TODO replace with parsing filters
        return list_shifts(
            response.text,
            earliest_start, latest_start
        )


    def get_user(self, id: int) -> User:
        response = self.client.get('/users', params={
            'action': 'view',
            'user_id': id,
        })
        response.raise_for_status()

        return get_user(response.text, id)
