# SPDX-License-Identifier: MIT

from ..schema import *
from ..utils import parse_date, parse_time, next_time
from bs4 import BeautifulSoup, Tag
from datetime import datetime, timedelta
from html import unescape
from typing import Iterator
from urllib.parse import parse_qs, urlparse
import re



## https://github.com/engelsystem/engelsystem/blob/v3.4.0/includes/view/ShiftCalendarRenderer.php#L21
_shift_margin_px = 5
_quater_block_height_px = 30

## https://github.com/engelsystem/engelsystem/blob/v3.4.0/includes/view/ShiftCalendarShiftRenderer.php#L263
_shift_title_regex = re.compile(r'^(?P<start_time>\d\d:\d\d) ‐ (?P<end_time>\d\d:\d\d) — (?P<shift_type>.+)$')

_shift_style_regex = re.compile(r'^height: (?P<height>\d+)px;')
_signup_regex = re.compile(r'^(?P<open_spots>\d+) (Helfer benötigt|helper needed)')


# @dataclass(frozen=True)
# class Filter:



def list_shifts(
    html: str,
    earliest_start: datetime, latest_start: datetime
) -> Iterator[Shift]:
    soup = BeautifulSoup(html, 'lxml')
    yield from _parse_shift_list(soup, earliest_start, latest_start)


# def parse_filter(doc: BeautifulSoup):

def _parse_shift_list(doc: BeautifulSoup, earliest_start: datetime, latest_start: datetime) -> Iterator[Shift]:
    time_table = doc.find('div', attrs={'class': 'shift-calendar'})
    if time_table is None:
        return []

    lanes = time_table.find_all('div', recursive=False)
    start, end = _parse_time_lane(lanes[0], earliest_start)

    for lane in lanes[1:]:
        yield from _parse_shift_lane(lane, start, end)


def _parse_time_lane(lane: Tag, earliest_start: datetime) -> tuple[datetime, datetime]:
    assert set(lane['class']) == set(['lane', 'time'])
    blocks = lane.find_all('div', recursive=False)

    header = blocks[0]
    assert 'header' in header['class']

    start = None
    last_hour = None
    blocks_since_last_hour = 0
    for block in blocks[1:]:
        assert 'tick' in block['class']

        title = list(block.stripped_strings)
        if len(title) == 0:
            blocks_since_last_hour += 1

        elif len(title) == 2:
            date, time = title

            date = parse_date(date)
            date = date.replace(year=earliest_start.year) ## TODO: Ugly hack does not deal with year changes
            time = parse_time(time)
            dt = datetime.combine(date, time)
            assert dt.minute == 0
            assert ('day' if dt.hour == 0 else 'hour') in block['class']

            if start is None:
                start = dt - timedelta(minutes=15*blocks_since_last_hour)
            else:
                assert blocks_since_last_hour == 3
                assert (dt - last_hour).total_seconds() == 60*60
            blocks_since_last_hour = 0
            last_hour = dt

        else:
            assert False

    ## Since there are two leading and two trailing blocks, at least one hour
    ## mark has to be present
    assert start is not None
    end = start + timedelta(minutes=15*(len(blocks[1:])))

    return start, end


def _parse_shift_lane(lane: Tag, start:datetime, end:datetime) -> Iterator[Shift]:
    assert lane['class'] == ['lane']
    blocks = lane.find_all('div', recursive=False)

    header = blocks[0]
    assert 'header' in header['class']
    location_link = header.find('a')
    query_string = urlparse(location_link['href']).query
    location_id = int(parse_qs(query_string)['location_id'][0])
    location_name = location_link.get_text(strip=True)
    location = Location(id=location_id, name=location_name)

    cur_dt = start
    for block in blocks[1:]:
        if block['class'] == ['shift-card']:
            m = _shift_style_regex.search(block['style'])
            ## https://github.com/engelsystem/engelsystem/blob/v3.4.0/includes/view/ShiftCalendarShiftRenderer.php#L43
            block_height = (int(m.group('height')) + _shift_margin_px) // _quater_block_height_px

            shift = _parse_shift(block, cur_dt, block_height, location)
            yield shift

            cur_dt += timedelta(minutes=15*block_height)

        elif 'tick' in block['class']:
            if cur_dt.minute == 0:
                assert ('day' if cur_dt.hour == 0 else 'hour') in block['class']
            cur_dt += timedelta(minutes=15)

        else:
            assert False

    assert cur_dt == end


def _parse_shift(shift_card: Tag, cur_dt: datetime, block_height: int, location: Location) -> Shift:
    ## HEADER
    header_link = shift_card.find('div', attrs={'class': 'card-header'}).a
    header_text = unescape(list(header_link.stripped_strings)[-1])

    m = _shift_title_regex.search(header_text)
    shift_type = m.group('shift_type')
    start_time = parse_time(m.group('start_time'))
    end_time   = parse_time(m.group('end_time'))

    start_dt = datetime.combine(cur_dt.date(), start_time)
    assert 0 <= (start_dt - cur_dt).total_seconds() < 15*60

    end_dt = start_dt + timedelta(minutes=15*block_height) - timedelta(hours=1)
    end_dt = next_time(end_dt, end_time)

    query_string = urlparse(header_link['href']).query
    shift_id = int(parse_qs(query_string)['shift_id'][0])

    ## BODY
    body = shift_card.find('div', attrs={'class': 'card-body'})
    title_icon = body.find('span', attrs={'class': 'bi-info-circle'})
    if title_icon is not None:
        shift_title = title_icon.nextSibling.strip()
    else:
        shift_title = None


    ## ANGEL LISTS
    jobs = {}
    for angel_list in (shift_card
        .find('ul', attrs={'class': 'list-group'})
        .find_all('li', attrs={'class': 'flex-wrap'}, recursive=False)
    ):
        ## Type
        angel_type_link = angel_list.strong.a
        angel_type_name = angel_type_link.get_text(strip=True)
        query_string = urlparse(angel_type_link['href']).query
        angel_type_id = int(parse_qs(query_string)['angeltype_id'][0])

        angel_type = AngelType(id=angel_type_id, name=angel_type_name)

        ## Angels
        angels = {}
        for angel_span in angel_list.find_all('span', recursive=False):
            angel_name = angel_span.get_text(strip=True)
            query_string = urlparse(angel_span.a['href']).query
            angel_id = int(parse_qs(query_string)['user_id'][0])

            angel = AngelColleague(
                id = angel_id,
                name = angel_name,
                is_freeloader = angel_span.a.parent.name == 'del'
            )
            angels[angel.id] = angel

        ## Open spots
        m = _signup_regex.search(list(angel_list.stripped_strings)[-2])
        if m is not None:
            open_spots = int(m.group('open_spots'))
        else:
            open_spots = 0

        jobs[angel_type.id] = Job(angel_type=angel_type, angels=angels, open_spots=open_spots)

    shift = Shift(
        id = shift_id,
        type = shift_type,
        title = shift_title,
        start = start_dt,
        end = end_dt,
        location = location,
        jobs = jobs
    )
    return shift
