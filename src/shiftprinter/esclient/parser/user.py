# SPDX-License-Identifier: MIT

from ..schema import *
from ..utils import parse_date, parse_time, next_time, parse_date_full, id_from_link, parse_datetime
from bs4 import BeautifulSoup, Tag
from datetime import datetime, timedelta
from typing import Iterator
from urllib.parse import parse_qs, urlparse



def get_user(html: str, id: int) -> User:
    soup = BeautifulSoup(html, 'lxml')

    ## === HEADER
    header = soup.find('h1')
    assert header.span['class'] == ['icon-icon_angel']

    pronoun = None
    if header.small:
        pronoun = header.small.get_text(strip=True)
        header.small.decompose()

    name = header.get_text(strip=True)
    angel_short = AngelShort(id=id, name=name)

    ## === USER INFO TILES
    user_infos = soup.find('div', {'class': 'user-info'})
    tiles = user_infos.find_all('div', recursive=False)

    ## Communication tile
    phone_numbers = []
    for row in tiles[0].find_all('h1', recursive=False):
        icon = row.find('span', {'class': 'bi'})

        if 'bi-phone' in icon['class']:
            phone_numbers.append(row.get_text(strip=True))

        elif 'bi-envelope' in icon['class']:
            pass

        else:
            raise Exception('Unknown communication row')

    ## Angel state
    for row in tiles[1].find_all('span', recursive=False):
        state_text = row.get_text(strip=True)

    ## Angel types
    angel_types = {}
    for angel_type_link in tiles[2].find_all('a', recursive=False):
        angel_type_id = id_from_link(angel_type_link, 'angeltype_id')
        angel_type_name = angel_type_link.get_text(strip=True)
        angel_types[angel_type_id] = AngelType(id=angel_type_id, name=angel_type_name)

    ## Angel rights
    roles = list(tiles[3].stripped_strings)[1:]

    ## === SHIFTS
    shifts = None
    worklogs = None
    shift_table = soup.find('tbody')
    if shift_table is not None:
        last_row = shift_table.contents[-1]
        match last_row.contents[0].get_text(strip=True):
            case 'Your goodie score™:':
                goodie_score = last_row.contents[1].get_text(strip=True)
            case 'Your t-shirt score™:':
                tshirt_score = last_row.contents[1].get_text(strip=True)
            case _:
                assert False
        last_row.decompose()

        total_row = shift_table.contents[-1]
        assert total_row.contents[0].get_text(strip=True) == 'Sum:'
        worked_hours = _parse_hours(total_row.contents[1].get_text(strip=True))
        total_row.decompose()

        ## Since we deleted all trailing special rows and the header is in
        ## <thead>, all remaining children in <tbody> are shift rows.
        shifts = {}
        worklogs = []
        for row in shift_table.contents:
            match len(list(row.contents[0].stripped_strings)):
                case 1:
                    worklogs.append(_parse_worklog(row.contents, angel_short))
                case 2:
                    shift_entry = _parse_shift_entry(row.contents, angel_short)
                    shifts[shift_entry.id] = shift_entry
                case _:
                    assert False

    ## === APIs
    api_key_element = soup.find('p', {'id': 'collapseApiKey'})
    api_key = api_key_element.get_text(strip=True) if api_key_element is not None else None


    return User(
        id = id,
        name = name,
        pronoun = pronoun,
        phone_numbers = phone_numbers,
        angel_types = angel_types,
        roles = roles,
        api_key = api_key,
        shifts = shifts,
        worklogs = worklogs
    )


def _parse_shift_entry(cells, angel_short):
    ## 1st cell: date and time
    date, time_span = list(cells[0].stripped_strings)
    start_time, end_time = time_span.split(' - ')

    date = parse_date_full(date)
    start_dt = datetime.combine(date, parse_time(start_time))
    end_dt = next_time(start_dt, parse_time(end_time))

    ## 2nd cell: duration
    score_duration = _parse_hours(cells[1].get_text(strip=True))

    ## 3rd cell: location
    location_link = cells[2].a
    location_id = id_from_link(location_link, 'location_id')
    location_name = location_link.get_text(strip=True)
    location = Location(id=location_id, name=location_name)

    ## 4th cell: shift & companions
    shift_type = None
    shift_title = None
    angle_type = None
    jobs = {}
    angel_colleagues = {}
    for link in cells[3].find_all('a'):
        queries = parse_qs(urlparse(link['href']).query)
        del queries['action']
        [(link_id_type, link_id)] = queries.items()
        link_id = int(link_id[0])
        link_text = link.get_text(strip=True)

        match link_id_type:
            case 'shift_id':
                if shift_type is None:
                    shift_type = link_text
                    shift_id = link_id
                elif shift_title is None:
                    shift_title = link_text
                else:
                    assert False

            case 'angeltype_id':
                if angle_type is not None:
                    job = Job(angel_type=angle_type, angels=angel_colleagues, open_spots=None)
                    jobs[job.angel_type.id] = job
                angle_type = AngelType(id=link_id, name=link_text)
                angel_colleagues = {}

            case 'user_id':
                ac = AngelColleague(
                    id = link_id,
                    name = link_text,
                    is_freeloader = link.parent.name == 'del'
                )
                angel_colleagues[ac.id] = ac

    if angle_type is not None:
        job = Job(angel_type=angle_type, angels=angel_colleagues, open_spots=None)
        jobs[job.angel_type.id] = job

    ## 5th cell: freeloding
    freeloading_p = cells[4].find('p')
    freeloading_comment = None
    if is_freeloaded := freeloading_p is not None:
        assert freeloading_p['class'] == ['text-danger']
        freeloading_prefix = 'Freeloaded: '
        freeloading_text = freeloading_p.get_text(strip=True)
        if freeloading_text.startswith(freeloading_prefix):
            freeloading_comment = freeloading_text[len(freeloading_prefix):]
        freeloading_p.decompose()

    comment = cells[4].get_text(strip=True)
    if comment == '':
        comment = None

    ## 6th cell: actions (shift entry ID)
    edit_button = cells[5].find('span', {'class': 'bi-pencil'}).parent
    shift_entry_id = id_from_link(edit_button, 'edit')

    return ShiftEntry(
        id = shift_entry_id,
        shift = Shift(
            id = shift_id,
            type = shift_type,
            title = shift_title,
            start = start_dt,
            end = end_dt,
            location = location,
            jobs = jobs
        ),
        score_duration = score_duration,
        angel = angel_short,
        comment = comment,
        is_freeloaded = is_freeloaded,
        freeloading_comment = freeloading_comment,
    )


def _parse_worklog(cells, angel_short):
    ## 1st cell: date and time
    date = parse_date_full(cells[0].get_text(strip=True))

    ## 2nd cell: duration
    score_duration = _parse_hours(cells[1].get_text(strip=True))

    ## 5th cell: comment
    comment = next(cells[4].stripped_strings)

    creator_link = cells[4].find('a')
    creator_id = id_from_link(creator_link, 'user_id')
    creator_name = creator_link.get_text(strip=True)
    creator = AngelShort(id=creator_id, name=creator_name)

    creation_date = creator_link.next_sibling.get_text(strip=True)
    assert creation_date.startswith('at ')
    creation_date = parse_datetime(creation_date[3:])

    ## 6th cell: actions (shift entry ID)
    if (pencil_icon := cells[5].find('span', {'class': 'bi-pencil'})) is not None:
        path = urlparse(pencil_icon.parent['href']).path.split('/')
        assert path[1] == 'admin' and path[2] == 'user' and path[4] == 'worklog'
        id = int(path[5])

    return Worklog(
        id = id,
        angel = angel_short,
        date = date,
        score_duration = score_duration,
        comment = comment,
        creator = creator,
        creation_date = creation_date
    )


def _parse_hours(text):
    assert text.endswith('\u00A0h') or text.endswith(' h')
    return timedelta(hours=float(text[:-2]))
