# SPDX-License-Identifier: MIT

from abc import ABCMeta, abstractmethod
from contextlib import suppress
from enum import Enum
from escpos import printer, escpos
from loguru import logger
from subprocess import run
from types import TracebackType
from typing import TypeVar, Any, Generic, Optional, get_args



## "Interface" of drivers
class DriverBase(metaclass=ABCMeta):
    @abstractmethod
    def __enter__(self) -> escpos.Escpos:
        pass

    @abstractmethod
    def __exit__(
        self,
        exc_type: Optional[type[BaseException]],
        exc_value: Optional[BaseException],
        traceback: Optional[TracebackType]
    ) -> None:
        pass


## Abstract (generic) base class of drivers
T = TypeVar('T', bound=type(escpos.Escpos))

class DriverGenericBase(DriverBase, Generic[T], metaclass=ABCMeta):
    def __init__(self, settings: dict[str, Any]):
        ## Instantiate escpos printer of type T
        self._device = get_args(type(self).__orig_bases__[0])[0](**settings) # type: ignore

    def __enter__(self) -> escpos.Escpos:
        ## Return freshly re-opened device in `with` block, so the hardware
        ## gets reinitialized after reconnect.
        logger.debug('Opening device')
        self._device.open()
        return self._device

    def __exit__(
        self,
        exc_type: Optional[type[BaseException]],
        exc_value: Optional[BaseException],
        traceback: Optional[TracebackType]
    ) -> None:
        if exc_type is None:
            logger.debug('Closing device')
            ## Close the device, so we savely can open it again later.
            self._device.close()
        else:
            ## If an exception happend during printing, try to cut off the half
            ## finished page. Also try to close it.
            logger.error('Failed to print. Tyring to cut page and close device.')
            with suppress(): self._device.cut()
            with suppress(): self._device.close()


## Implementations of drivers
class UsbDriver(DriverGenericBase[printer.Usb]): pass
class SerialDriver(DriverGenericBase[printer.Serial]): pass
class NetworkDriver(DriverGenericBase[printer.Network]): pass
class FileDriver(DriverGenericBase[printer.File]): pass
class LpDriver(DriverGenericBase[printer.LP]): pass
class CupsDriver(DriverGenericBase[printer.CupsPrinter]): pass
class NullDriver(DriverGenericBase[printer.Dummy]): pass

class ConvertDriver(DriverGenericBase[printer.Dummy]):
    def __init__(self, settings: dict[str, Any]):
        self.converter = settings['converter']
        super().__init__({})

    def __exit__(
        self,
        exc_type: Optional[type[BaseException]],
        exc_value: Optional[BaseException],
        traceback: Optional[TracebackType]
    ) -> None:
        super().__exit__(exc_type, exc_value, traceback)
        ret = run(self.converter, shell=True, input=self._device.output)
        if ret.returncode != 0:
            logger.error(
                f'Failed to convert ESC/POS data on stdin using "{self.converter}" (exit code {ret.returncode})'
            )


## Collection of drivers
class Driver(Enum):
    usb     = UsbDriver
    serial  = SerialDriver
    network = NetworkDriver
    file    = FileDriver
    lp      = LpDriver
    cups    = CupsDriver
    null    = NullDriver
    convert = ConvertDriver
