"""
This type stub file was generated by pyright.
"""

import six
from abc import ABCMeta
from typing import List, Literal, Optional, Union

"""Main class.

This module contains the abstract base class :py:class:`Escpos`.

:author: python-escpos developers
:organization: Bashlinux and `python-escpos <https://github.com/python-escpos>`_
:copyright: Copyright (c) 2012-2017 Bashlinux and python-escpos
:license: MIT
"""
HW_BARCODE_NAMES = ...
SW_BARCODE_NAMES = ...
@six.add_metaclass(ABCMeta)
class Escpos:
    """ESC/POS Printer object.

    This class is the abstract base class for an Esc/Pos-printer. The printer implementations are children of this
    class.
    """
    _device: Union[Literal[False], Literal[None], object] = ...
    def __init__(self, profile=..., magic_encode_args=..., **kwargs) -> None:
        """Initialize ESCPOS Printer.

        :param profile: Printer profile
        """
        ...
    
    def __del__(self): # -> None:
        """Call self.close upon deletion."""
        ...
    
    @property
    def device(self) -> Union[Literal[None], object]:
        """Implements a self-open mechanism.

        An attempt to get the property before open the connection
        will cause the connection to open.
        """
        ...
    
    @device.setter
    def device(self, new_device: Union[Literal[False], Literal[None], object]): # -> None:
        ...
    
    def open(self): # -> None:
        """Open a printer device/connection."""
        ...
    
    def image(self, img_source, high_density_vertical=..., high_density_horizontal=..., impl=..., fragment_height=..., center=...) -> None:
        """Print an image.

        You can select whether the printer should print in high density or not. The default value is high density.
        When printing in low density, the image will be stretched.

        Esc/Pos supplies several commands for printing. This function supports three of them. Please try to vary the
        implementations if you have any problems. For example the printer `IT80-002` will have trouble aligning
        images that are not printed in Column-mode.

        The available printing implementations are:

            * `bitImageRaster`: prints with the `GS v 0`-command
            * `graphics`: prints with the `GS ( L`-command
            * `bitImageColumn`: prints with the `ESC *`-command

        When trying to center an image make sure you have initialized the printer with a valid profile, that
        contains a media width pixel field. Otherwise the centering will have no effect.

        :param img_source: PIL image or filename to load: `jpg`, `gif`, `png` or `bmp`
        :param high_density_vertical: print in high density in vertical direction *default:* True
        :param high_density_horizontal: print in high density in horizontal direction *default:* True
        :param impl: choose image printing mode between `bitImageRaster`, `graphics` or `bitImageColumn`
        :param fragment_height: Images larger than this will be split into multiple fragments *default:* 960
        :param center: Center image horizontally *default:* False

        """
        ...
    
    def qr(self, content: str, ec: int=..., size:int=..., model: int=..., native: bool=..., center: bool=..., impl: bool=...) -> None:
        """Print QR Code for the provided string.

        :param content: The content of the code. Numeric data will be more efficiently compacted.
        :param ec: Error-correction level to use. One of QR_ECLEVEL_L (default), QR_ECLEVEL_M, QR_ECLEVEL_Q or
            QR_ECLEVEL_H.
            Higher error correction results in a less compact code.
        :param size: Pixel size to use. Must be 1-16 (default 3)
        :param model: QR code model to use. Must be one of QR_MODEL_1, QR_MODEL_2 (default) or QR_MICRO (not supported
            by all printers).
        :param native: True to render the code on the printer, False to render the code as an image and send it to the
            printer (Default)
        :param center: Centers the code *default:* False
        :param impl: Image-printing-implementation, refer to :meth:`.image()` for details
        """
        ...
    
    def charcode(self, code: str = ...) -> None:
        """Set Character Code Table.

        Sets the control sequence from ``CHARCODE`` in :py:mod:`escpos.constants` as active.
        It will be sent with the next text sequence.
        If you set the variable code to ``AUTO`` it will try to automatically guess the
        right codepage.
        (This is the standard behavior.)

        :param code: Name of CharCode
        :raises: :py:exc:`~escpos.exceptions.CharCodeError`
        """
        ...
    
    @staticmethod
    def check_barcode(bc: str, code: str) -> bool: # -> Match[str] | Literal[False] | None:
        """Check if barcode is OK.

        This method checks if the barcode is in the proper format.
        The validation concerns the barcode length and the set of characters, but won't compute/validate any checksum.
        The full set of requirement for each barcode type is available in the ESC/POS documentation.

        As an example, using EAN13, the barcode `12345678901` will be correct, because it can be rendered by the
        printer. But it does not suit the EAN13 standard, because the checksum digit is missing. Adding a wrong
        checksum in the end will also be considered correct, but adding a letter won't (EAN13 is numeric only).

        .. todo:: Add a method to compute the checksum for the different standards

        .. todo:: For fixed-length standards with mandatory checksum (EAN, UPC),
            compute and add the checksum automatically if missing.

        :param bc: barcode format, see :py:meth:`.barcode()`
        :param code: alphanumeric data to be printed as bar code, see :py:meth:`.barcode()`
        :return: bool
        """
        ...
    
    def barcode(self, code, bc, height: int = ..., width: int = ..., pos: str = ..., font: str = ..., align_ct: bool = ..., function_type=..., check: bool = ..., force_software: Union[bool, str] = ...) -> None:
        """Print barcode.

        Automatic hardware|software barcode renderer according to the printer capabilities.

        Defaults to hardware barcode and its format types if supported.
        Automatically switches to software barcode renderer if hardware does not
        support a barcode type that is supported by software. (e.g. JAN, ISSN, etc.).

        Set force_software=True to force the software renderer according to the profile.
        Set force_software=graphics|bitImageColumn|bitImageRaster to specify a renderer.

        Ignores caps, special chars and whitespaces in barcode type names.
        So "EAN13", "ean-13", "Ean_13", "EAN 13" are all accepted.

        :param code: alphanumeric data to be printed as bar code (payload).

        :param bc: barcode format type (EAN13, CODE128, JAN, etc.).

        :param height: barcode module height (in printer dots), has to be between 1 and 255.
            *default*: 64

        :param width: barcode module width (in printer dots), has to be between 2 and 6.
            *default*: 3

        :param pos: text position (ABOVE, BELOW, BOTH, OFF) relative to the barcode
            (ignored in software renderer).
            *default*: BELOW

        :param font: select font A or B (ignored in software renderer).
            *default*: A

        :param align_ct: If *True*, center the barcode.
            *default*: True

        :param function_type: ESCPOS function type A or B. None to guess it from profile
            (ignored in software renderer).
            *default*: None

        :param check: If *True*, checks that the code meets the requirements of the barcode type.
            *default*: True

        :param force_software: If *True*, force the use of software barcode renderer from profile.
            If *"graphics", "bitImageColumn" or "bitImageRaster"*, force the use of specific renderer.

        :raises: :py:exc:`~escpos.exceptions.BarcodeCodeError`,
                 :py:exc:`~escpos.exceptions.BarcodeTypeError`

        .. note::
            Get all supported formats at:
              - Hardware: :py:const:`~escpos.constants.BARCODE_FORMATS`
              - Software:
                `Python barcode documentation <https://python-barcode.readthedocs.io/en/stable/supported-formats.html>`_
        """
        ...
    
    def text(self, txt: str) -> None: # -> None:
        """Print alpha-numeric text.

        The text has to be encoded in the currently selected codepage.
        The input text has to be encoded in unicode.

        :param txt: text to be printed
        :raises: :py:exc:`~escpos.exceptions.TextError`
        """
        ...
    
    def textln(self, txt: str=...) -> None: # -> None:
        """Print alpha-numeric text with a newline.

        The text has to be encoded in the currently selected codepage.
        The input text has to be encoded in unicode.

        :param txt: text to be printed with a newline
        :raises: :py:exc:`~escpos.exceptions.TextError`
        """
        ...
    
    def ln(self, count: int=...) -> None: # -> None:
        """Print a newline or more.

        :param count: number of newlines to print
        :raises: :py:exc:`ValueError` if count < 0
        """
        ...
    
    def block_text(self, txt, font=..., columns=...) -> None: # -> None:
        """Print text wrapped to specific columns.

        Text has to be encoded in unicode.

        :param txt: text to be printed
        :param font: font to be used, can be :code:`a` or :code:`b`
        :param columns: amount of columns
        :return: None
        """
        ...
    
    def set(self, align: Optional[str] = ..., font: Optional[str] = ..., bold: Optional[bool] = ..., underline: Optional[int] = ..., width: Optional[int] = ..., height: Optional[int] = ..., density: Optional[int] = ..., invert: Optional[bool] = ..., smooth: Optional[bool] = ..., flip: Optional[bool] = ..., normal_textsize: Optional[bool] = ..., double_width: Optional[bool] = ..., double_height: Optional[bool] = ..., custom_size: Optional[bool] = ...) -> None:
        """Set text properties by sending them to the printer.

        If a value for a parameter is not supplied, nothing is sent
        for this type of format.

        :param align: horizontal position for text, possible values are:

            * 'center'
            * 'left'
            * 'right'

        :param font: font given as an index, a name, or one of the
            special values 'a' or 'b', referring to fonts 0 and 1.
        :param bold: text in bold
        :param underline: underline mode for text, decimal range 0-2
        :param normal_textsize: switch to normal text size if True
        :param double_height: doubles the height of the text
        :param double_width: doubles the width of the text
        :param custom_size: uses custom size specified by width and height
            parameters. Cannot be used with double_width or double_height.
        :param width: text width multiplier when custom_size is used, decimal range 1-8
        :param height: text height multiplier when custom_size is used, decimal range 1-8
        :param density: print density, value from 0-8, if something else is supplied the density remains unchanged
        :param invert: True enables white on black printing
        :param smooth: True enables text smoothing. Effective on 4x4 size text and larger
        :param flip: True enables upside-down printing
        """
        ...
    
    def set_with_default(self, align: Optional[str] = ..., font: Optional[str] = ..., bold: Optional[bool] = ..., underline: Optional[int] = ..., width: Optional[int] = ..., height: Optional[int] = ..., density: Optional[int] = ..., invert: Optional[bool] = ..., smooth: Optional[bool] = ..., flip: Optional[bool] = ..., double_width: Optional[bool] = ..., double_height: Optional[bool] = ..., custom_size: Optional[bool] = ...) -> None:
        """Set default text properties by sending them to the printer.

        This function has the behavior of the `set()`-method from before
        version 3.
        If a parameter to this method is not supplied, a default value
        will be sent.
        Otherwise this method forwards the values to the
        :py:meth:`escpos.Escpos.set()`.

        :param align: horizontal position for text, possible values are:

            * 'center'
            * 'left'
            * 'right'

            *default*: 'left'

        :param font: font given as an index, a name, or one of the
            special values 'a' or 'b', referring to fonts 0 and 1.
        :param bold: text in bold, *default*: False
        :param underline: underline mode for text, decimal range 0-2,  *default*: 0
        :param double_height: doubles the height of the text
        :param double_width: doubles the width of the text
        :param custom_size: uses custom size specified by width and height
            parameters. Cannot be used with double_width or double_height.
        :param width: text width multiplier when custom_size is used, decimal range 1-8,  *default*: 1
        :param height: text height multiplier when custom_size is used, decimal range 1-8, *default*: 1
        :param density: print density, value from 0-8, if something else is supplied the density remains unchanged
        :param invert: True enables white on black printing, *default*: False
        :param smooth: True enables text smoothing. Effective on 4x4 size text and larger, *default*: False
        :param flip: True enables upside-down printing, *default*: False
        """
        ...
    
    def line_spacing(self, spacing: Optional[int] = ..., divisor: int = ...) -> None:
        """Set line character spacing.

        If no spacing is given, we reset it to the default.

        There are different commands for setting the line spacing, using
        a different denominator:

        '+'' line_spacing/360 of an inch, 0 <= line_spacing <= 255
        '3' line_spacing/180 of an inch, 0 <= line_spacing <= 255
        'A' line_spacing/60 of an inch, 0 <= line_spacing <= 85

        Some printers may not support all of them. The most commonly
        available command (using a divisor of 180) is chosen.
        """
        ...
    
    def cut(self, mode: str = ..., feed: bool = ...) -> None:
        """Cut paper.

        Without any arguments the paper will be cut completely. With 'mode=PART' a partial cut will
        be attempted. Note however, that not all models can do a partial cut. See the documentation of
        your printer for details.

        :param mode: set to 'PART' for a partial cut. default: 'FULL'
        :param feed: print and feed before cutting. default: true
        :raises ValueError: if mode not in ('FULL', 'PART')
        """
        ...
    
    def cashdraw(self, pin) -> None:
        """Send pulse to kick the cash drawer.

        Kick cash drawer on pin 2 (:py:const:`~escpos.constants.CD_KICK_2`)
        or pin 5 (:py:const:`~escpos.constants.CD_KICK_5`)
        according to the default parameters.
        For non default parameters send a decimal sequence i.e.
        [27,112,48] or [27,112,0,25,255].

        :param pin: pin number, 2 or 5 or list of decimals
        :raises: :py:exc:`~escpos.exceptions.CashDrawerError`
        """
        ...
    
    def linedisplay_select(self, select_display: bool = ...) -> None:
        """Select the line display or the printer.

        This method is used for line displays that are daisy-chained between your computer and printer.
        If you set `select_display` to true, only the display is selected and if you set it to false,
        only the printer is selected.

        :param select_display: whether the display should be selected or the printer
        """
        ...
    
    def linedisplay_clear(self) -> None:
        """Clear the line display and resets the .

        This method is used for line displays that are daisy-chained between your computer and printer.
        """
        ...
    
    def linedisplay(self, text: str) -> None:
        """Display text on a line display connected to your printer.

        You should connect a line display to your printer. You can do this by daisy-chaining
        the display between your computer and printer.

        :param text: Text to display
        """
        ...
    
    def hw(self, hw: str) -> None:
        """Hardware operations.

        :param hw: hardware action, may be:

            * INIT
            * SELECT
            * RESET
        """
        ...
    
    def print_and_feed(self, n: int = ...) -> None:
        """Print data in print buffer and feed *n* lines.

        If n not in range (0, 255) then a ValueError will be raised.

        :param n: number of n to feed. 0 <= n <= 255. default: 1
        :raises ValueError: if not 0 <= n <= 255
        """
        ...
    
    def control(self, ctl: str, count: int = ..., tab_size: int = ...) -> None:
        """Feed control sequences.

        :param ctl: string for the following control sequences:

            * LF *for Line Feed*
            * FF *for Form Feed*
            * CR *for Carriage Return*
            * HT *for Horizontal Tab*
            * VT *for Vertical Tab*

        :param count: integer between 1 and 32, controls the horizontal tab count. Defaults to 5.
        :param tab_size: integer between 1 and 255, controls the horizontal tab size in characters. Defaults to 8
        :raises: :py:exc:`~escpos.exceptions.TabPosError`
        """
        ...
    
    def panel_buttons(self, enable: bool = ...) -> None:
        """Control the panel buttons on the printer (e.g. FEED).

        When enable is set to False the panel buttons on the printer
        will be disabled.
        Calling the method with `enable=True` or without argument
        will enable the panel buttons.

        If panel buttons are enabled, the function of the panel button,
        such as feeding, will be executed upon pressing the button.
        If the panel buttons are disabled, pressing them will not have
        any effect.

        This command is effective until the printer is initialized,
        resetted or power-cycled.
        The default is enabled panel buttons.

        Some panel buttons will always work, especially when the
        printer is opened.
        See for more information the manual of your printer and
        the escpos-command-reference.

        :param enable: controls the panel buttons
        """
        ...
    
    def query_status(self, mode: bytes) -> List[int]:
        """Query the printer for its status.

        Returns an array of integers containing it.

        :param mode: Integer that sets the status mode queried to the printer.
            - RT_STATUS_ONLINE: Printer status.
            - RT_STATUS_PAPER: Paper sensor.
        """
        ...
    
    def is_online(self) -> bool:
        """Query the online status of the printer.

        :returns: When online, returns ``True``; ``False`` otherwise.
        """
        ...
    
    def paper_status(self): # -> Literal[2, 0, 1] | None:
        """Query the paper status of the printer.

        Returns 2 if there is plenty of paper, 1 if the paper has arrived to
        the near-end sensor and 0 if there is no paper.

        :returns: 2: Paper is adequate. 1: Paper ending. 0: No paper.
        """
        ...
    
    def target(self, type: str = ...) -> None:
        """Select where to print to.

        Print to the thermal printer by default (ROLL) or
        print to the slip dot matrix printer if supported (SLIP)
        """
        ...
    
    def eject_slip(self) -> None:
        """Eject the slip/cheque."""
        ...
    
    def print_and_eject_slip(self) -> None:
        """Print and eject.

        Prints data from the buffer to the slip station and if the paper
        sensor is covered, reverses the slip out the front of the printer
        far enough to be accessible to the operator.
        The impact station opens the platen in all cases.
        """
        ...
    
    def use_slip_only(self) -> None:
        """Select the Slip Station for all functions.

        The receipt station is the default setting after the printer
        is initialized or the Clear Printer (0x10) command is received
        """
        ...
    
    def buzzer(self, times: int = ..., duration: int = ...) -> None:
        """Activate the internal printer buzzer on supported printers.

        The 'times' parameter refers to the 'n' escpos command parameter,
        which means how many times the buzzer will be 'beeped'.

        :param times: Integer between 1 and 9, indicates the buzzer beeps.
        :param duration: Integer between 1 and 9, indicates the beep duration.
        :returns: None
        """
        ...
    


class EscposIO:
    r"""ESC/POS Printer IO object.

    Allows the class to be used together with the `with`-statement. You have to define a printer instance
    and assign it to the EscposIO class.
    This example explains the usage:

    .. code-block:: Python

        with EscposIO(printer.Serial('/dev/ttyUSB0')) as p:
            p.set(font='a', height=2, align='center', text_type='bold')
            p.printer.set(align='left')
            p.printer.image('logo.gif')
            p.writelines('Big line\\n', font='b')
            p.writelines('Привет')
            p.writelines('BIG TEXT', width=2)

    After the `with`-statement the printer automatically cuts the paper if `autocut` is `True`.
    """
    def __init__(self, printer: Escpos, autocut: bool = ..., autoclose: bool = ..., **kwargs) -> None:
        """Initialize object.

        :param printer: An EscPos-printer object
        :param autocut: If True, paper is automatically cut after the `with`-statement *default*: True
        :param kwargs: These arguments will be passed to :py:meth:`escpos.Escpos.set()`
        """
        ...
    
    def set(self, **kwargs) -> None:
        """Set the printer-parameters.

        Controls which parameters will be passed to :py:meth:`Escpos.set() <escpos.escpos.Escpos.set()>`.
        For more information on the parameters see the :py:meth:`set() <escpos.escpos.Escpos.set()>`-methods
        documentation. These parameters can also be passed with this class' constructor or the
        :py:meth:`~escpos.escpos.EscposIO.writelines()`-method.

        :param kwargs: keyword-parameters that will be passed to :py:meth:`Escpos.set() <escpos.escpos.Escpos.set()>`
        """
        ...
    
    def writelines(self, text, **kwargs): # -> None:
        """Print text."""
        ...
    
    def close(self): # -> None:
        """Close printer.

        Called upon closing the `with`-statement.
        """
        ...
    
    def __enter__(self, **kwargs): # -> Self@EscposIO:
        """Enter context."""
        ...
    
    def __exit__(self, type, value, traceback): # -> None:
        """Cut and close if configured.

        If :py:attr:`autocut <escpos.escpos.EscposIO.autocut>` is `True` (set by this class' constructor),
        then :py:meth:`printer.cut() <escpos.escpos.Escpos.cut()>` will be called here.
        """
        ...
    


