"""
This type stub file was generated by pyright.
"""

from typing import Any
from ..escpos import Escpos

"""This module contains the implementation of the Win32Raw printer driver.

:author: python-escpos developers
:organization: `python-escpos <https://github.com/python-escpos>`_
:copyright: Copyright (c) 2012-2023 Bashlinux and python-escpos
:license: MIT
"""
_DEP_WIN32PRINT = ...
_DEP_WIN32PRINT = ...
PyPrinterHANDLE: Any = ...
def is_usable() -> bool:
    """Indicate whether this component can be used due to dependencies."""
    ...

def dependency_win32print(func): # -> _Wrapped[..., Unknown, (*args: Unknown, **kwargs: Unknown), Unknown]:
    """Indicate dependency on win32print."""
    ...

class Win32Raw(Escpos):
    """Printer binding for win32 API.

    Uses the module pywin32 for printing.

    inheritance:

    .. inheritance-diagram:: escpos.printer.Win32Raw
        :parts: 1

    """
    @staticmethod
    def is_usable() -> bool:
        """Indicate whether this printer class is usable.

        Will return True if dependencies are available.
        Will return False if not.
        """
        ...
    
    @dependency_win32print
    def __init__(self, printer_name: str = ..., *args, **kwargs) -> None:
        """Initialize default printer."""
        ...
    
    @property
    def printers(self) -> dict:
        """Available Windows printers."""
        ...
    
    def open(self, job_name: str = ..., raise_not_found: bool = ...) -> None:
        """Open connection to default printer.

        By default raise an exception if device is not found.

        :param raise_not_found: Default True.
                                False to log error but do not raise exception.

        :raises: :py:exc:`~escpos.exceptions.DeviceNotFoundError`
        """
        ...
    
    def close(self) -> None:
        """Close connection to default printer."""
        ...
    


